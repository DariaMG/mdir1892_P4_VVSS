package note.repository;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

import note.utils.ClasaException;
import note.utils.Constants;

import note.model.Nota;

public class NoteRepositoryMock implements NoteRepository {
  private List<Nota> note;
  private String fisier;

  public NoteRepositoryMock(String fisier) {
    note = new LinkedList<Nota>();
    this.fisier = fisier;
  }


  @Override
  public void addNota(Nota nota) throws ClasaException {
    // TODO Auto-generated method stub
    if (!validareNota(nota))
      return;
    note.add(nota);
    writeNote(fisier);
  }

  private boolean validareNota(Nota nota) throws ClasaException {
    // TODO Auto-generated method stub
    if (nota.getMaterie().length() < 5 || nota.getMaterie().length() > 20)
      throw new ClasaException(Constants.invalidMateria);
    if (nota.getNrmatricol() < Constants.minNrmatricol || nota.getNrmatricol() > Constants.maxNrmatricol)
      throw new ClasaException(Constants.invalidNrmatricol);
    if (nota.getNota() < Constants.minNota || nota.getNota() > Constants.maxNota)
      throw new ClasaException(Constants.invalidNota);
    if (nota.getNota() != (int) nota.getNota())
      throw new ClasaException(Constants.invalidNota);
    if (nota.getNrmatricol() != (int) nota.getNrmatricol())
      throw new ClasaException(Constants.invalidNrmatricol);
    return true;
  }

  @Override
  public List<Nota> getNote() {
    // TODO Auto-generated method stub
    return note;
  }

  public void readNote(String fisier) {
    try {
      FileInputStream fstream = new FileInputStream(fisier);
      DataInputStream in = new DataInputStream(fstream);
      BufferedReader br = new BufferedReader(new InputStreamReader(in));
      String line;
      while ((line = br.readLine()) != null) {
        String[] values = line.split(";");
        Nota nota = new Nota(Double.parseDouble(values[0]), values[1], Double.parseDouble(values[2]));
        note.add(nota);
      }
      br.close();
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void writeNote(String fisier) {
    BufferedWriter bw = null;
    FileWriter fw = null;

    try {

      fw = new FileWriter(fisier);
      bw = new BufferedWriter(fw);
      for(Nota n:note) {
        bw.write(n.getNrmatricol()+";"+n.getMaterie()+";"+n.getNota()+"\n");
      }

      System.out.println("Done");

    } catch (IOException e) {

      e.printStackTrace();

    } finally {

      try {

        if (bw != null)
          bw.close();

        if (fw != null)
          fw.close();

      } catch (IOException ex) {

        ex.printStackTrace();

      }
    }

  }
}
