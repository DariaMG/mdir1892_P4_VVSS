package note.utils;

public class ClasaException extends Exception {

    private static final long serialVersionUID = 1460943029226542682L;

    public ClasaException(){}

    public ClasaException(String message) {
        super(message);
    }

}

